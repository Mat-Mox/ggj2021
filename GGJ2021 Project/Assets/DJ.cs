using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DJ : NetworkBehaviour
{
    private FCGameManager gm;

    [SerializeField] private AudioClip lobby;
    [SerializeField] private AudioClip search;
    [SerializeField] private AudioClip bring;

    private AudioSource source;
    
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<FCGameManager>();
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.partyState == EPartyState.Waiting)
        {
            if (source.clip != lobby)
            {
                source.Stop();
                source.clip = lobby;
                source.Play();
            }
        }

        if (gm.partyState == EPartyState.Starting)
        {
            if (source.clip != search)
            {
                source.Stop();
                source.clip = search;
                source.Play();
            }

        }

        if (gm.partyState == EPartyState.Bringing)
        {
            
            if (source.clip != bring)
            {
                source.Stop();
                source.clip = bring;
                source.Play();
            }

        }
    }
}
