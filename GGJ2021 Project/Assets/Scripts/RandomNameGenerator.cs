using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RandomNameGenerator : MonoBehaviour
{

    public static string[] firstName = {
        "Super",
        "Old",
        "Young",
        "little",
        "Mac",
        "Big",
        "Lord",
        "Soldier",
        "Bat",
        "Epic",
        "Gigantic",
        "French",
        "Dieu",
        "Dragon",
        "Cortex",
        "Fanta",
        "Anonymouse",
        "Ostie",
        "Meme",
        "Monkey",
        "Spider",
        "Kikou",

    };
    public static string[] lastName = { 

        "Kiki",
        "Steve",
        "42",
        "76",
        "Man",
        "ReverseMan",
        "Mat",
        "Dinguerie",
        "Al'",
        "Monster",
        "Kiss",
        "BBen",
        "Harumta",
        "Ajirog",
        "God",
        "du93",
        "DesPyramides",
        "Bob",
        "Wilfoufou",
        "Jung�Mvp�JongHyun",
        "PewDiePie",

    };
    private static string Kikou = "Xx_";
    private static string Kikou1 = "_xX";
    private static string finalName;

    public static string RandomName()
    {
        var number = Random.Range(1, firstName.Length)-1;
        var number2 = Random.Range(1, lastName.Length)-1;

        finalName = firstName[number] + "" + lastName[number2];


        int a = Random.Range(0, 10);
        if (a == 0)
        {
            finalName = Kikou + finalName + Kikou1;
        }

        return finalName;
    }
}
