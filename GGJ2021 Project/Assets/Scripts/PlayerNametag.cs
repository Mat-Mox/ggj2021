using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TMPro;

public class PlayerNametag : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private TextMeshProUGUI usernametext;
    [SerializeField] private FC_CharaController player;

    // Update is called once per frame
    void Update()
    {
        canvas.transform.LookAt(FindObjectOfType<Camera>().transform);
        usernametext.text = player.name;
    }
}
