using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Chrono : MonoBehaviour
{
    [SerializeField] float chrono = 0.0f;
    private TextMeshProUGUI timer = null;

    private void Start()
    {
        timer = GetComponent<TextMeshProUGUI>();
        //timer.SetText(chrono.ToString());
    }

    private void Update()
    {
        if (chrono < 0)
        {
            //timer.SetText("00:00");
        }
        else
        {
            chrono -= Time.deltaTime;
            //timer.SetText((chrono).ToString("F2"));
        }
    }
}
