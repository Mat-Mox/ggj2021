using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class FCGameManager : NetworkBehaviour
{
    [SyncVar] public EPartyState partyState = EPartyState.Waiting;

    [SyncVar] public FC_CharaController crownOwner;

    public int idCounter = 0;

    void Start()
    {
        if (isServer)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    
    /// <summary>
    /// everyone is ready starting the search
    /// </summary>
    [Server]
    public void StartGame()
    {
        partyState = EPartyState.Starting;
        // resets everyone
        
        foreach (var VARIABLE in FindObjectsOfType<FC_CharaController>())
        {
            Respawn(VARIABLE);
        }

        StartCoroutine(LateStartGame(3));

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    [ClientRpc]
    void Respawn(FC_CharaController player)
    {
        player.Respawn();
    }

    IEnumerator LateStartGame(float t)
    {
        yield return new WaitForSeconds(t);
        LateStartGame();
    }

    [Server]
    void LateStartGame()
    {
        partyState = EPartyState.Searching;
        FindObjectOfType<spawnCrown>().SpawnCrown();
    }
    
    /// <summary>
    /// Called by a player when he gets the crown
    /// </summary>
    /// <param name="player">Reference to the Player script</param>
    public void CrownFound(FC_CharaController player)
    {
        if (partyState == EPartyState.Searching)
        {
            partyState = EPartyState.Bringing;
        }

        crownOwner = player;
        
        RpcCrownFound(player);
    }
    
    [ClientRpc]
    void RpcCrownFound(FC_CharaController player)
    {
        //print(player.id + " has the crown");
    }
    
    public void CrownBringedBack(FC_CharaController player)
    {
        if (player == crownOwner)
        {
            partyState = EPartyState.Ending;
            
            FindObjectOfType<spawnCrown>().HideCrown();

            AnnounceWinner(player);

            StartCoroutine(RestartLobbyIn(10));
        }
    }

    IEnumerator RestartLobbyIn(float t)
    {
        yield return new WaitForSeconds(t);
        RestartLobby();
    }

    [Server]
    void RestartLobby()
    {
        partyState = EPartyState.Waiting;
        RpcDeactivateWinScreen();
        foreach (var VARIABLE in FindObjectsOfType<FC_CharaController>())
        {
            Respawn(VARIABLE);
        }

        crownOwner = null;
    }

    [ClientRpc]
    void RpcDeactivateWinScreen()
    {
        FindObjectOfType<WinScreen>().DeactivateWinScreen();
        if (isServer)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    
    public void LooseCrown()
    {
        if (partyState == EPartyState.Bringing)
            crownOwner = null;
    }

    [ClientRpc]
    void AnnounceWinner(FC_CharaController player)
    {
        print(player.name + " WINS");
        
        FindObjectOfType<WinScreen>().ActivateWinScreen();
        FindObjectOfType<WinScreen>().SetWinnerNameText(player.name);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    /// <summary>
    /// Called by the player at connection to get an unique id
    /// </summary>
    /// <returns></returns>
    public int GetNextID()
    {
        return idCounter++;
    }

    public string GetNextName()
    {
        return RandomNameGenerator.RandomName();
    }
}

public enum EPartyState
{
    Waiting, // Lobby
    Starting, // Intro
    Searching, // Crown hidden
    Bringing, // Crown not hidden anymore
    Ending // Results + auto restart
}
