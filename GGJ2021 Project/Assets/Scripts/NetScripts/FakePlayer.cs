using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Random = UnityEngine.Random;

public class FakePlayer : NetworkBehaviour
{
    private float lastTime = 0;
    private float cd = 1;
    private FCGameManager gameManager;
    public string name;
    [SyncVar] public int id;
    [SerializeField] private Canvas testCanvas;
        
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<FCGameManager>();
        CmdAskForID();
        if (!isLocalPlayer)
        {
            Destroy(testCanvas.gameObject);
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.partyState == EPartyState.Searching || gameManager.partyState == EPartyState.Bringing)
        {
            // moves the player randomly every {cd} seconds
            if (Time.timeSinceLevelLoad - lastTime > cd)
            {
                transform.Translate(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
                lastTime = Time.timeSinceLevelLoad;
            }
        }
    }

    [Command]
    void CmdAskForID()
    {
        id = gameManager.GetNextID();
    }

    [Command]
    public void CmdGetCrown()
    {
        if (gameManager.partyState == EPartyState.Searching || gameManager.partyState == EPartyState.Bringing)
        {
            //gameManager.CrownFound(this);
        }
    }

    [Command]
    public void CmdBringCrown()
    {
        if (gameManager.partyState == EPartyState.Bringing)
        {
            //gameManager.CrownBringedBack(this);
        }
    }
}
