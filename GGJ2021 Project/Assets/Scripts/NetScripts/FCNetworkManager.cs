using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class FCNetworkManager : NetworkManager
{
    void Start()
    {
        if (MainMenu.fromMenu)
        {
            if (MainMenu.isHosting)
            {
                StartHost();
            }
            else
            {
                networkAddress = MainMenu.IP;
                StartClient();
            }
        }
        else
        {
            GetComponent<NetworkManagerHUD>().showGUI = true;
        }
    }
    
    public override void OnServerDisconnect (NetworkConnection conn) 
    {
        //print(conn.identity.gameObject.GetComponent<FC_CharaController>().id + "ded");
        if (conn.identity.gameObject.GetComponent<FC_CharaController>() == FindObjectOfType<FCGameManager>().crownOwner)
            FindObjectOfType<FCGameManager>().LooseCrown();
        Destroy(conn.identity.gameObject);
    }
    
    public override void OnClientDisconnect (NetworkConnection conn)
    {
        MainMenu.LoadMenu();
    }
}
