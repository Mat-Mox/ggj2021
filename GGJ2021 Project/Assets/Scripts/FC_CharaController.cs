using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.InputSystem;


public class FC_CharaController : NetworkBehaviour
{
    [SerializeField] private Animator animator;
    private NetworkAnimator networkAnimator;

    [Header("Network")]
    private FCGameManager gameManager;
    [SyncVar] public string name;
    [SyncVar] public int id;
    [SerializeField] private GameObject cameraSystem;
    [SerializeField] private PlayerInput playerInput;

    [Header("Movement")]
    [SerializeField]
    private float moveSpeed = 10;
    private Vector2 inputMovement = Vector2.zero;
    private Vector3 moveDirection = Vector3.zero;

    private Vector3 lookPosition = Vector3.zero;
    private Vector2 lastInput = Vector2.zero;
    
    [Space]

    [Header("Jump")]
    [SerializeField]
    private float jumpForce = 10;

    [SerializeField] private float distanceToGroundCheck = 1.1f;
    private bool isGrounded = false;

    private bool isJumping = true;
    private bool jump = false;

    private bool isStun = false;
    private float timeHit = 0;
    [SerializeField]
    private float stunTime = 2;

    [Space]
    [Header("CmdHit")]
    [SerializeField]
    private float punchForce = 10;
    private bool canPunch = true;
    [SerializeField] private float punchBaseCoolDown = 1;
    private float punchCoolDown;
    private bool isHiting = false;
    private RaycastHit[] punchHit;

    // Player component
    private Rigidbody rigidBody = null;
    [SerializeField]
    private Collider collider = null;
    [SerializeField]
    private GameObject body = null;
    [SerializeField]
    private Transform cameraPivot = null;

    private Vector3 spawnPosition;
    private Quaternion spawnRotation;

    private MenuInGame menuCanvas;

    // Start is called before the first frame update
    void Start()
    {
        networkAnimator = GetComponent<NetworkAnimator>();
        if (!isLocalPlayer)
        {
            playerInput.enabled = false;
            Destroy(cameraSystem);
            Destroy(GetComponent<PlayerInput>());
        }

        menuCanvas = FindObjectOfType<MenuInGame>();

        spawnPosition = transform.position;
        spawnRotation = transform.rotation;
        
        gameManager = FindObjectOfType<FCGameManager>();
        CmdAskForID();
        CmdAskForName();

        if (isLocalPlayer)
        {
            playerInput.enabled = true;
        }
        
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.constraints = RigidbodyConstraints.FreezeRotation;

        lookPosition = body.transform.position + body.transform.forward;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            animator.SetFloat("speed", rigidBody.velocity.magnitude);
            animator.SetBool("stun", isStun);
        }
    }
    
    [Command]
    void CmdAskForID()
    {
        id = gameManager.GetNextID();
    }
    
    [Command]
    void CmdAskForName()
    {
        name = gameManager.GetNextName();
    }

    [Command]
    public void CmdGetCrown()
    {
        if (gameManager.partyState == EPartyState.Searching || gameManager.partyState == EPartyState.Bringing)
        {
            gameManager.CrownFound(this);
        }
    }

    [Command]
    public void CmdBringCrown()
    {
        if (gameManager.partyState == EPartyState.Bringing)
        {
            gameManager.CrownBringedBack(this);
        }
    }

    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            if (rigidBody.velocity.magnitude > .2f)
            {
                SoundWalk();
            }
            else
            {
                SoundStopWalk();
            }
            
            if (gameManager.partyState == EPartyState.Waiting || gameManager.partyState == EPartyState.Searching || gameManager.partyState == EPartyState.Bringing)
            {
                // If the player have the control (Movement + Jump)
                if (!isStun)
                {
                    moveDirection = (cameraPivot.forward * inputMovement.y + cameraPivot.right * inputMovement.x) * moveSpeed + cameraPivot.up * rigidBody.velocity.y;

                    rigidBody.velocity = moveDirection;
                    
                    //Setup the point to look at
                    if(inputMovement != Vector2.zero)
                    {
                        if(lastInput != inputMovement)
                        {
                            lastInput = inputMovement;
                        }
                        lookPosition = body.transform.position + new Vector3(moveDirection.x, 0, moveDirection.z) ;
                    } 
                    else
                    {
                        lookPosition.y = body.transform.position.y;
                    }


                    //Smooth look at direction of movement
                    body.transform.LookAt(Vector3.Lerp(body.transform.position + body.transform.forward, lookPosition, 0.02f));
                    
                    if (jump && IsGrounded())
                    {
                        rigidBody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                        networkAnimator.SetTrigger("jump");
                        isJumping = true;
                        jump = false;
                    }
                    //print(IsGrounded());

                    if (isHiting)
                    {
                        punchHit = Physics.BoxCastAll(body.transform.position + body.transform.forward,
                            new Vector3(.5f, .5f, .5f), body.transform.forward, transform.rotation, .5f);

                        if (punchHit.Length != 0)
                            TryPunch();
                        
                        isHiting = false;
                        canPunch = false;
                        punchCoolDown = punchBaseCoolDown;
                    }

                    if (!canPunch)
                    {
                        punchCoolDown -= Time.deltaTime;
                        if (punchCoolDown < 0)
                        {
                            canPunch = true;
                        }
                    }
                } 
                else
                {
                    if(Time.time >= timeHit)
                    {
                        isStun = false;
                        timeHit = 0;
                        //print("I'm feeling better");
                    }

                    //print("Oh no I'm stunned : " + this.gameObject);
                }
            }

        }
        else
        {
            
        }
    }
    
    // Check if the player touch the ground
    private bool IsGrounded()
    {
        Debug.DrawLine(transform.position, transform.position + Vector3.down * (distanceToGroundCheck), Color.white);
        if (Physics.Raycast(transform.position, Vector3.down, distanceToGroundCheck))
        {
            isJumping = false;
            return true;
        } 
        else
        {
            return false;
        }
    }

    private void TryPunch()
    {
        SoundWoosh();
        foreach(RaycastHit hit in punchHit){

            if(hit.collider.gameObject != this.gameObject)
            {
                //print("One Punch !!!! on : " + hit.collider.gameObject);
                if (hit.collider.gameObject.tag == "Player")
                {
                    Vector3 direction = hit.collider.gameObject.transform.position - transform.position;
                    CmdHit(hit.collider.gameObject, direction);
                }
            } 
            else
            {
                //print("my bad it's me");
            }
        }
    }

    [Command]
    private void CmdHit(GameObject other, Vector3 direction)
    {
        //print(other.name + " say OOF !!");

        if (other.GetComponent<FC_CharaController>() == gameManager.crownOwner)
        {
            gameManager.LooseCrown();
            print(id + "lostthecrown");
        }

        RpcHit(other, direction);
    }

    [ClientRpc]
    private void RpcHit(GameObject other, Vector3 direction)
    {
        other.GetComponent<FC_CharaController>().GetKnocked(direction.normalized * punchForce);
    }

    void GetKnocked(Vector3 v3)
    {
        if (!isStun)
        {
            SoundHit();
            GetComponent<Rigidbody>().AddForce(v3, ForceMode.VelocityChange);
            Stun();
        }
    }

    public void Stun()
    {
        isStun = true;
        timeHit = Time.time + stunTime;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(body.transform.position + body.transform.forward, Vector3.one);
    }
    
    //Get the value of the movement input
    public void GetMovementValues(InputAction.CallbackContext context)
    {
        inputMovement = context.ReadValue<Vector2>();
        //print(inputMovement);
    }

    //Get if a jump input is pressed
    public void GetJumpInput(InputAction.CallbackContext context)
    {
        jump = context.performed;
    }

    //Get if the player hit
    public void GetHitInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            if (canPunch)
            {
                networkAnimator.SetTrigger("punch");
                isHiting = true;
                //print(id + " punched!");
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Crown") && gameManager.crownOwner == null)
        {
            CmdGetTheCrown();
        }

        if (other.gameObject.CompareTag("WinZone") && gameManager.crownOwner == this)
        {
            CmdBringedBack();
        }
    }

    [Command]
    void CmdBringedBack()
    {
        gameManager.CrownBringedBack(this);
    }

    [Command]
    void CmdGetTheCrown()
    {
        gameManager.CrownFound(this);
    }

    
    public void Respawn()
    {
        rigidBody.velocity = Vector3.up;
        transform.position = spawnPosition;
        transform.rotation = spawnRotation;
    }

    public void SetPause(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            menuCanvas.ToggleMenu();
        }
    }

    #region Sounds

    [Header("Sound")] 
    [SerializeField] private AudioSource shoutsource;
    [SerializeField] private List<AudioClip> shoutClips;
    [SerializeField] private AudioSource walksource;
    [SerializeField] private AudioSource wooshsource;
    [SerializeField] private AudioSource hitsource;
    
    void SoundShout()
    {
        if (isServer)
        {
            RpcSoundShout();
        }
        else
        {
            CmdSoundShout();
        }
    }

    [Command]
    void CmdSoundShout()
    {
        RpcSoundShout();
    }
    
    [ClientRpc]
    void RpcSoundShout()
    {
        shoutsource.clip = shoutClips[Random.Range(0, shoutClips.Count)];
        shoutsource.Play();
    }

    void SoundWalk()
    {
        if (isServer)
        {
            RpcSoundWalk();
        }
        else
        {
            CmdSoundWalk();
        }
    }

    [Command]
    void CmdSoundWalk()
    {
        RpcSoundWalk();
    }
    
    [ClientRpc]
    void RpcSoundWalk()
    {
        if (!walksource.isPlaying)
        {
            walksource.pitch = Random.Range(.5f, .8f);
            walksource.Play();
        }
    }
    
    void SoundStopWalk()
    {
        if (isServer)
        {
            RpcSoundStopWalk();
        }
        else
        {
            CmdSoundStopWalk();
        }
    }

    [Command]
    void CmdSoundStopWalk()
    {
        RpcSoundStopWalk();
    }
    
    [ClientRpc]
    void RpcSoundStopWalk()
    {
        walksource.Stop();
    }

    void SoundWoosh()
    {
        if (isServer)
        {
            RpcSoundWoosh();
        }
        else
        {
            CmdSoundWoosh();
        }
    }

    [Command]
    void CmdSoundWoosh()
    {
        RpcSoundWoosh();
    }
    
    [ClientRpc]
    void RpcSoundWoosh()
    {
        wooshsource.pitch = Random.Range(.8f, 1.2f);
        wooshsource.Play();
    }
    
    void SoundHit()
    {
        if (isServer)
        {
            RpcSoundHit();
        }
        else
        {
            CmdSoundHit();
        }
    }

    [Command]
    void CmdSoundHit()
    {
        RpcSoundHit();
    }
    
    [ClientRpc]
    void RpcSoundHit()
    {
        hitsource.pitch = Random.Range(.8f, 1.2f);
        hitsource.Play();
    }

    #endregion
}
