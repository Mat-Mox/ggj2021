using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;
using TMPro;
using UnityEditor;

public class MainMenu : MonoBehaviour       //script � mettre dans PauseMenu
{
    public string levelToLoad;

    public static bool fromMenu;
    
    public static bool isHosting;
    public static string IP;
    
    [SerializeField] TMP_InputField ipText;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    public void HostGame()
    {
        isHosting = true;
        fromMenu = true;
        SceneManager.LoadScene(levelToLoad);
    }

    public void JoinGame()
    {
        if (CheckIPValid(ipText.text))
        {
            isHosting = false;
            fromMenu = true;
            IP = ipText.text;
            SceneManager.LoadScene(levelToLoad);
        }
        else
        {
            print("Wrong IP format");
        }
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }

    private static bool CheckIPValid(string strIP)
    {
        return Regex.IsMatch(strIP, "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    }

    public static void LoadMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
