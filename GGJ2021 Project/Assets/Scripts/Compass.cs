using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Mirror;
using UnityEngine.UI;

public class Compass : NetworkBehaviour        //Script � mettre sur le player
{
    private Transform Crown;

    private FCGameManager gameManager;
    public GameObject Arrow;

    private bool enable = false;

    void Start()
    {
        Crown = GameObject.FindGameObjectWithTag("Crown").transform;
        gameManager = FindObjectOfType<FCGameManager>();
        Arrow.SetActive(false);
    }
    
    void Update()
    {
        if (isLocalPlayer)
        {
            if (gameManager.partyState == EPartyState.Bringing)
            {
                if (gameManager.crownOwner == null)
                {
                    enable = true;
                }
                else
                {
                    if (gameManager.crownOwner.gameObject != gameObject)
                    {
                        enable = true;
                    }
                    else
                    {
                        enable = false;
                    }
                }

            }
            
            Arrow.transform.LookAt(Crown);

            if(enable)
            {
                Arrow.SetActive(true);
            }
            else
            {
                Arrow.SetActive(false);
            }
        }
    }
}
